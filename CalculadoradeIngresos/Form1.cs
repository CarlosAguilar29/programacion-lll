﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calculadoradeIngresos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Aguinaldo()
        {
            double sueldo = Double.Parse(txtSueldo.Text);
            double nomina = Double.Parse(txtNomina.Text);
            double resultado;

            resultado = (sueldo / nomina) * 15;

            lblRAguinaldo.Text = "$" + Convert.ToString(Math.Round(resultado, 2));
        }

        private void Vacaciones()
        {
            double sueldo = Double.Parse(txtSueldo.Text);
            double nomina = Double.Parse(txtNomina.Text);
            double vacaciones = Double.Parse(txtVacaciones.Text);
            double resultado;

            resultado = ((sueldo / nomina) * vacaciones) * 0.25;

            lblRVacaciones.Text = "$" + Convert.ToString(Math.Round(Math.Round(resultado, 2), 2));
        }
        private void infonavit()
        {
            double sueldo = Double.Parse(txtSueldo.Text);
            double nomina = Double.Parse(txtNomina.Text);
            double resultado;

            resultado = sueldo * 0.05;

            lblRInfonavit.Text = "$" + Convert.ToString(Math.Round(resultado, 2));
        }
        private void IMSS()
        {
            double sueldo = Double.Parse(txtSueldo.Text);
            double nomina = Double.Parse(txtNomina.Text);
            double resultado;

            double auxA, auxB, auxC;

            auxA = sueldo * 0.0025;
            auxB = sueldo * 0.00375;
            auxC = sueldo * 0.00625;

            resultado = auxA + auxB + auxC;

            lblRIMSS.Text = "$" + Convert.ToString(Math.Round(resultado, 2));
        }
        private void RCV()
        {
            double sueldo = Double.Parse(txtSueldo.Text);
            double nomina = Double.Parse(txtNomina.Text);
            double resultado;

            resultado = (sueldo * 0.01125) * 2;

            lblRRCV.Text = "$" + Convert.ToString(Math.Round(resultado, 2));
        }
        private void Sueldo_diario()
        {
            double sueldo = Double.Parse(txtSueldo.Text);
            double nomina = Double.Parse(txtNomina.Text);
            double resultado;


            resultado = sueldo / nomina;

            lblRSueldo.Text = "$" + Convert.ToString(Math.Round(resultado, 2));
        }
        private void despensa()
        {
            double sueldo = Double.Parse(txtSueldo.Text);
            double nomina = Double.Parse(txtNomina.Text);
            double resultado;

            resultado = sueldo * 0.01;

            lblRVales.Text = "$" + Convert.ToString(Math.Round(resultado, 2));
        }
        private void SGMM()
        {
            double sueldo = Double.Parse(txtSueldo.Text);
            double nomina = Double.Parse(txtNomina.Text);
            double resultado;
        }
        private void seguro()
        {
            double sueldo = Double.Parse(txtSueldo.Text);
            double nomina = Double.Parse(txtNomina.Text);
            double resultado;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtNomina.Text) || string.IsNullOrEmpty(txtVacaciones.Text) || string.IsNullOrEmpty(txtSueldo.Text))
            {
                MessageBox.Show("Ningún campo debe estar vacío.");
            }
            else
            {
                Aguinaldo();
                Vacaciones();
                infonavit();
                IMSS();
                RCV();
                Sueldo_diario();
                despensa();
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtNomina.Text = "";
            txtVacaciones.Text = "";
            txtSueldo.Text = "";

            lblRAguinaldo.Text = "$";
            lblRVacaciones.Text = "$";
            lblRSueldo.Text = "$";
            lblRInfonavit.Text = "$";
            lblRIMSS.Text = "$";
            lblRRCV.Text = "$";
            lblRVales.Text = "$";
            lblRSeguro.Text = "$";
            lblRSGMM.Text = "$";
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}



