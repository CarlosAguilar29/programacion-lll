import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class SockMerchant {

    public static int sockMerchant(int n, int[] ar){
        int counter = 0;
        if(ar.length == 0) return counter;

        Set<Integer> set = new HashSet();
        for(int i=0; i<ar.length; i++){
            if(!set.contains(ar[i])){
                set.add(ar[i]);
            }else{
                counter++;
                set.remove(ar[i]);
            }
        }
        return counter;
    }

   public static Scanner scan = new Scanner(System.in);

    public static void main(String[] args){
        int n;
        System.out.println("Introduce socks amount: ");
         n = scan.nextInt();
        int [] ar = new int[n];
        for(int i=0; i<n; i++){
            System.out.println("Introduce color: ");
            ar[i] = scan.nextInt();
        }
        for(int i=0; i<n; i++){
            System.out.print(ar[i] + ",");
        }

        int result = sockMerchant(n,ar);
        System.out.println(" ");
        System.out.println(result);
    }
}
