﻿namespace Relog_checador
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEmpleado = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.radBtnEntrada = new System.Windows.Forms.RadioButton();
            this.radBtnSalida = new System.Windows.Forms.RadioButton();
            this.cmbEmpleado = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.grpOperacion = new System.Windows.Forms.GroupBox();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.btnRevisar = new System.Windows.Forms.Button();
            this.grpOperacion.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblEmpleado
            // 
            this.lblEmpleado.AutoSize = true;
            this.lblEmpleado.Location = new System.Drawing.Point(40, 60);
            this.lblEmpleado.Name = "lblEmpleado";
            this.lblEmpleado.Size = new System.Drawing.Size(54, 13);
            this.lblEmpleado.TabIndex = 0;
            this.lblEmpleado.Text = "Empleado";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Location = new System.Drawing.Point(40, 122);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(65, 13);
            this.lblFecha.TabIndex = 1;
            this.lblFecha.Text = "Fecha/Hora";
            // 
            // radBtnEntrada
            // 
            this.radBtnEntrada.AutoSize = true;
            this.radBtnEntrada.Location = new System.Drawing.Point(3, 19);
            this.radBtnEntrada.Name = "radBtnEntrada";
            this.radBtnEntrada.Size = new System.Drawing.Size(62, 17);
            this.radBtnEntrada.TabIndex = 4;
            this.radBtnEntrada.TabStop = true;
            this.radBtnEntrada.Text = "Entrada";
            this.radBtnEntrada.UseVisualStyleBackColor = true;
            this.radBtnEntrada.CheckedChanged += new System.EventHandler(this.radBtnEntrada_CheckedChanged);
            // 
            // radBtnSalida
            // 
            this.radBtnSalida.AutoSize = true;
            this.radBtnSalida.Location = new System.Drawing.Point(118, 19);
            this.radBtnSalida.Name = "radBtnSalida";
            this.radBtnSalida.Size = new System.Drawing.Size(54, 17);
            this.radBtnSalida.TabIndex = 5;
            this.radBtnSalida.TabStop = true;
            this.radBtnSalida.Text = "Salida";
            this.radBtnSalida.UseVisualStyleBackColor = true;
            this.radBtnSalida.CheckedChanged += new System.EventHandler(this.radBtnSalida_CheckedChanged);
            // 
            // cmbEmpleado
            // 
            this.cmbEmpleado.FormattingEnabled = true;
            this.cmbEmpleado.Items.AddRange(new object[] {
            "Luis Carlos Aguilar Huerta",
            "Carlos Ivan Beltran Puga",
            "Cesar Basulto Velez",
            "Antonio Diego Bravo",
            "Edgar Omar Perez Herrera"});
            this.cmbEmpleado.Location = new System.Drawing.Point(115, 57);
            this.cmbEmpleado.Name = "cmbEmpleado";
            this.cmbEmpleado.Size = new System.Drawing.Size(239, 21);
            this.cmbEmpleado.TabIndex = 6;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(115, 116);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(239, 20);
            this.dateTimePicker1.TabIndex = 7;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // grpOperacion
            // 
            this.grpOperacion.Controls.Add(this.radBtnEntrada);
            this.grpOperacion.Controls.Add(this.radBtnSalida);
            this.grpOperacion.Location = new System.Drawing.Point(115, 169);
            this.grpOperacion.Name = "grpOperacion";
            this.grpOperacion.Size = new System.Drawing.Size(213, 44);
            this.grpOperacion.TabIndex = 8;
            this.grpOperacion.TabStop = false;
            this.grpOperacion.Text = "Operacion";
            this.grpOperacion.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Enabled = false;
            this.btnRegistrar.Location = new System.Drawing.Point(118, 255);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(75, 23);
            this.btnRegistrar.TabIndex = 9;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // btnRevisar
            // 
            this.btnRevisar.Enabled = false;
            this.btnRevisar.Location = new System.Drawing.Point(233, 255);
            this.btnRevisar.Name = "btnRevisar";
            this.btnRevisar.Size = new System.Drawing.Size(75, 23);
            this.btnRevisar.TabIndex = 10;
            this.btnRevisar.Text = "Revisar";
            this.btnRevisar.UseVisualStyleBackColor = true;
            this.btnRevisar.Click += new System.EventHandler(this.btnRevisar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(415, 400);
            this.Controls.Add(this.btnRevisar);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.grpOperacion);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.cmbEmpleado);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.lblEmpleado);
            this.Name = "Form1";
            this.Text = "Registrador";
            this.grpOperacion.ResumeLayout(false);
            this.grpOperacion.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblEmpleado;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.RadioButton radBtnEntrada;
        private System.Windows.Forms.RadioButton radBtnSalida;
        private System.Windows.Forms.ComboBox cmbEmpleado;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.GroupBox grpOperacion;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Button btnRevisar;
    }
}

