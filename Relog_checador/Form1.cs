﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace Relog_checador
{
    public partial class Form1 : Form
    {   DateTime input = new DateTime();
        DateTime output = new DateTime();
        DateTime data = new DateTime();

        string s;

        bool a = false;
        bool b = false;
        int aux = 0;

        public string location = @"C:\Users\Carlos Aguilar\desktop\Registrador.txt";
        public Form1()
        {
            InitializeComponent();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            input = dateTimePicker1.Value;
        }

        private void radBtnEntrada_CheckedChanged(object sender, EventArgs e)
        {
            input = dateTimePicker1.Value;
            btnRegistrar.Enabled = true;
            btnRevisar.Enabled = true;
            b = true;
            aux = 1;
        }

        private void btnRevisar_Click(object sender, EventArgs e)
        {
            string readText = File.ReadAllText(location);
            MessageBox.Show(readText);

        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            s = cmbEmpleado.Text;

            if (!File.Exists(location))
            {
                using (StreamWriter sw = File.CreateText(location))
                {
                    sw.WriteLine(s);
                    sw.WriteLine(input);

                }
            }
            else
            {
                string append = "";
                if (aux == 1)
                {
                    append = "Registro de Entrada\n" + s + "\n" + input + Environment.NewLine;
                }
                else if (aux == 2)
                {
                    append = "Registro de Salida\n" + s + "\n" + output + Environment.NewLine;
                }

                File.AppendAllText(location, append);
                input = data;

            }
        }

        private void radBtnSalida_CheckedChanged(object sender, EventArgs e)
        {
            output = dateTimePicker1.Value;
            btnRegistrar.Enabled = true;
            btnRevisar.Enabled = true;
            a = true;
            aux = 2;
        }
    }
}
